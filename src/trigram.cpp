#include <trigram.h>

#include <cstring>
#include <stdexcept>

static_assert(3u == sizeof(Trigram), "Trigram must have size 3");

bool Trigram::operator<(const Trigram& other) const {
    return std::strncmp(m_data, other.m_data, 3) < 0;
}

const char& Trigram::operator[](size_t position) const {
    if (position >= 3u) {
        throw std::out_of_range("Trigram position access out of range (>= 3)");
    }

    return m_data[position];
}

std::ostream& operator<<(std::ostream& os, const Trigram& tg) {
    os << tg[0] << tg[1] << tg[2];
    return os;
}
