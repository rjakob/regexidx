#include <index.h>

int main(int argc, const char** argv) {
    Index idx;

    for (int i=1;i<argc;++i) {
        std::string txt(argv[i]);
        idx.addString(i, txt);
    }

    idx.printIndex();

    idx.writeFile("/tmp/index.txt");

    return 0;
}
