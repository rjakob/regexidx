#include <index.h>

#include <iostream>
#include <fstream>

Index::Index() {
}

void Index::addString(unsigned int key, const std::string& string) {
    for (unsigned int pos=0; pos <= string.length()-3; ++pos) {
        Trigram trigram(string.substr(pos, 3));

        m_index.insert({trigram, std::make_pair(key, pos)});
    }
}

void Index::printIndex() {
    for (const auto& key_value : m_index) {
        std::cout << key_value.first << ": " << key_value.second.second << std::endl;
    }

    IndexType::iterator start, end;
    std::tie(start,end) = m_index.equal_range("ist");

    std::cout << std::endl << "Result for \"ist\"" << std::endl;
    for (;start != end; ++start) {
        std::cout << start->second.first << "," << start->second.second << std::endl;
    }
}

std::vector<Index::PositionType> Index::find(const Trigram& tr) {
    IndexType::iterator start, end;
    std::tie(start,end) = m_index.equal_range(tr);

    std::vector<Index::PositionType> result;
    for (; start != end; ++start) {
        result.emplace_back(start->second);
    }

    return result;
}

void Index::writeFile(const std::string& filename) {
    std::ofstream fs;
    fs.open(filename);

    for (const auto& key_value : m_index) {

        const std::string key = std::to_string(key_value.second.first);
        const std::string pos = std::to_string(key_value.second.second);

        fs.write(&key_value.first[0], 3);
        fs.write("\t", 1);
        fs.write(key.c_str(), key.size());
        fs.write("\t", 1);
        fs.write(pos.c_str(), pos.size());
        fs.write( "\n", 1 );
    }

    fs.close();
}

void Index::readFile(const std::string& filename) {
}
