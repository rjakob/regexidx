#include <gtest/gtest.h>
#include <trigram.h>
#include <stdexcept>

TEST(Trigram, TrigramAccess) {
    Trigram t{"Test"};

    EXPECT_EQ('T', t[0]);
    EXPECT_EQ('e', t[1]);
    EXPECT_EQ('s', t[2]);
    ASSERT_THROW(t[3], std::out_of_range);
}

TEST(Trigram, TrigramLessThan) {
    Trigram t1("Tes");
    Trigram t2("Tet");

    EXPECT_LT(t1, t2);
    EXPECT_FALSE(t2 < t1);
}

TEST(Trigram, TrigramOperatorWrite) {
    Trigram tr("Tes");
    std::stringstream s;

    s << tr;

    EXPECT_STREQ("Tes", s.str().c_str());
}
