#ifndef REGEXIDX_INCLUDE_H
#define REGEXIDX_INCLUDE_H

#include <map>
#include <regex>
#include <string>
#include <vector>

#include <trigram.h>

class Index
{
public:
    using PositionType = std::pair<unsigned int, std::string::size_type>;

    Index();

    //! Add a new string with some key to find it again to the index.
    void addString(unsigned int key, const std::string& string);

    //! Find the position with a trigram
    std::vector<PositionType> find(const Trigram& tr);

// Debug stuff

    void printIndex();
    void writeFile(const std::string& filename);
    void readFile(const std::string& filename);

private:
    using IndexType = std::multimap<const Trigram, const PositionType>;
    IndexType m_index;
};

#endif // REGEXIDX_INCLUDE_H
