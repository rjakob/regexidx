#ifndef REGEXIDX_TRIGRAM_H
#define REGEXIDX_TRIGRAM_H

#include <iostream>

class Trigram {
public:
    Trigram(const char* c) {
        m_data[0] = c[0];
        m_data[1] = c[1];
        m_data[2] = c[2];
    }

    Trigram(const std::string& str) {
        m_data[0] = str[0];
        m_data[1] = str[1];
        m_data[2] = str[2];
    }

    const char& operator[](size_t position) const;
    bool operator<(const Trigram& other) const;

private:
    char m_data[3];
};

std::ostream& operator<<(std::ostream& os, const Trigram& tg);

#endif // REGEXIDX_TRIGRAM_H
